/*
  Yi3 Themes: "Happy" (extends 2013 "Masonry" Theme by Clean Themes UK)
  Copyright (C) Various Authors
*/

/**********************************************************************
                            THIRD-PARTY
**********************************************************************/

/*
 * Support for jQuery css bezier animation.
 *
 * @author Jonah Fox
 * @source Shopify Theme: Simple (2013)
 */
(function($){

  $.path = {};

  var V = {
    rotate: function(p, degrees) {
      var radians = degrees * Math.PI / 180,
        c = Math.cos(radians),
        s = Math.sin(radians);
      return [c*p[0] - s*p[1], s*p[0] + c*p[1]];
    },
    scale: function(p, n) {
      return [n*p[0], n*p[1]];
    },
    add: function(a, b) {
      return [a[0]+b[0], a[1]+b[1]];
    },
    minus: function(a, b) {
      return [a[0]-b[0], a[1]-b[1]];
    }
  };

  $.path.bezier = function( params, rotate ) {
    params.start = $.extend( {angle: 0, length: 0.3333}, params.start );
    params.end = $.extend( {angle: 0, length: 0.3333}, params.end );

    this.p1 = [params.start.x, params.start.y];
    this.p4 = [params.end.x, params.end.y];

    var v14 = V.minus( this.p4, this.p1 ),
      v12 = V.scale( v14, params.start.length ),
      v41 = V.scale( v14, -1 ),
      v43 = V.scale( v41, params.end.length );

    v12 = V.rotate( v12, params.start.angle );
    this.p2 = V.add( this.p1, v12 );

    v43 = V.rotate(v43, params.end.angle );
    this.p3 = V.add( this.p4, v43 );

    this.f1 = function(t) { return (t*t*t); };
    this.f2 = function(t) { return (3*t*t*(1-t)); };
    this.f3 = function(t) { return (3*t*(1-t)*(1-t)); };
    this.f4 = function(t) { return ((1-t)*(1-t)*(1-t)); };

    /* p from 0 to 1 */
    this.css = function(p) {
      var f1 = this.f1(p), f2 = this.f2(p), f3 = this.f3(p), f4=this.f4(p), css = {};
      if (rotate) {
        css.prevX = this.x;
        css.prevY = this.y;
      }
      css.x = this.x = ( this.p1[0]*f1 + this.p2[0]*f2 +this.p3[0]*f3 + this.p4[0]*f4 +.5 )|0;
      css.y = this.y = ( this.p1[1]*f1 + this.p2[1]*f2 +this.p3[1]*f3 + this.p4[1]*f4 +.5 )|0;
      css.left = css.x + "px";
      css.top = css.y + "px";
      return css;
    };
  };

  $.path.arc = function(params, rotate) {
    for ( var i in params ) {
      this[i] = params[i];
    }

    this.dir = this.dir || 1;

    while ( this.start > this.end && this.dir > 0 ) {
      this.start -= 360;
    }

    while ( this.start < this.end && this.dir < 0 ) {
      this.start += 360;
    }

    this.css = function(p) {
      var a = ( this.start * (p ) + this.end * (1-(p )) ) * Math.PI / 180,
        css = {};

      if (rotate) {
        css.prevX = this.x;
        css.prevY = this.y;
      }
      css.x = this.x = ( Math.sin(a) * this.radius + this.center[0] +.5 )|0;
      css.y = this.y = ( Math.cos(a) * this.radius + this.center[1] +.5 )|0;
      css.left = css.x + "px";
      css.top = css.y + "px";
      return css;
    };
  };

  $.fx.step.path = function(fx) {
    var css = fx.end.css( 1 - fx.pos );
    if ( css.prevX != null ) {
      $.cssHooks.transform.set( fx.elem, "rotate(" + Math.atan2(css.prevY - css.y, css.prevX - css.x) + ")" );
    }
    fx.elem.style.top = css.top;
    fx.elem.style.left = css.left;
  };

})(jQuery);

/*
 * Extend jQuery with methdo to aniamte height or width to auto.
 *
 * @param {String} [selector] CSS selector that matches "Add to Cart" button(s).
 * @param {Function} [callback] Callback function.
 *
 * @return {Object} jQuery object of matched elements.
 */
jQuery.fn.animateAuto = function(prop, speed, callback){
  var elem = $(),
      height = '',
      width = '',
      ret = $();

  ret = this.each(function(i, el){
    el = jQuery(el),
    elem = el.clone().css({'height':'auto','width':'auto','visibility':'hidden'}).removeAttr('id').appendTo(el.parent());
    height = elem.css('height'),
    width = elem.css('width');
    elem.remove();

    if (prop === 'height') {
      el.animate({'height': height}, speed, callback);
    } else if (prop === 'width') {
      el.animate({'width': width}, speed, callback);  
    } else if (prop === 'both') {
      el.animate({'width': width,'height':height}, speed, callback);
    }
  });

  return ret;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
  Yi3 Themes: "Happy" (extends 2013 "Masonry" Theme by Clean Themes UK)
  Copyright (C) 2013 Yi3 (except where otherwise noted)
*/

/**********************************************************************
                        Yi3 THEMES "HAPPY"
**********************************************************************/

/* PROPERTIES */

/*
 * All cart-based UX provided by Yi3 Themes for the theme "Happy"
 *
 * @class
 * @author Wesley B (Yi3 Themes)
 */
function Yi3Cart () {};

/* HANDLE EVENTS */

/*
 * Animate adding a product to the cart.
 *
 * @param {String} [selector] CSS selector that matches "Add to Cart" button(s).
 * @param {Function} [callback] Callback function.
 *
 * @author Shopify
 * @source Shopify Theme: Simple (2013)
 * @author Wesley B (Yi3 Themes)
 */
Yi3Cart.prototype.handleAddToCart = function (selector, callback) {
    var self = this;

    $(selector).click(function (e) {

        var elem = $(this),
            el = {
              html: $('html'),
              cartAnimation: $('#cart-animation'),
              cartTarget: $('.cartsummary'),
            }

        $(elem).prop('disabled', true);

        e.preventDefault();

        function animate(id, callback) {

            el.cartAnimation.show()

            var timeoutID = window.setTimeout(renableButton, 1500);

            function renableButton() {

            }

            var addtocartWidth = elem.outerWidth() / 2
            var addtocartHeight = elem.outerHeight() / 2

            var addtocartLeft = elem.offset().left + addtocartWidth;
            var addtocartTop = elem.offset().top + addtocartHeight;

            var buttonAreaWidth = el.cartTarget.outerWidth();
            var buttonAreaHeight = el.cartTarget.outerHeight();

            var buttonAreaLeft = el.cartTarget.offset().left + buttonAreaWidth / 2 - el.cartAnimation.outerWidth() / 2;

            var htmlMargin = el.html.css('margin-top')
            var htmlMarginTrim = parseInt(htmlMargin);

            if (htmlMargin !== 0) {
                var buttonAreaTop = el.cartTarget.offset().top + buttonAreaHeight / 2 - htmlMarginTrim - el.cartAnimation.outerHeight() / 2;
            } else {
                var buttonAreaTop = el.cartTarget.offset().top + buttonAreaHeight / 2 - el.cartAnimation.outerHeight() / 2;
            }

            var path = {
                start: {
                    x: addtocartLeft,
                    y: addtocartTop,
                    angle: 190.012,
                    length: 0.2
                },
                end: {
                    x: buttonAreaLeft,
                    y: buttonAreaTop,
                    angle: 90.012,
                    length: 0.50
                }
            };

            el.cartAnimation.animate({
                    path: new $.path.bezier(path)
                },{
                    duration: 1200,
                    complete: function () {
                        elem.prop('disabled', false);
                        el.cartAnimation.fadeOut(500);
                        if (typeof callback === 'function') {
                            callback.apply(self, [id]);
                        } else {
                            elem.closest('form').submit();
                        }
                    }
                }
            );
        }

        animate(this.id, callback);
    });
};

/* RELOAD XML */

/*
 * Reload cart items on cart page.
 *
 * @param {Function} [callback] Callback function.
 */
Yi3Cart.prototype.reloadCart = function (callback) {
  var ids = {
        loadingEl: 'yi3---loading',
        cartFormItems: 'cartform-items'
      },
      selectors = {
        cartFormItems: '#' + ids.cartFormItems,
        cartFormItemsContent: '#' + ids.cartFormItems + ' > ul'
      },
      cartFormItems = jQuery(selectors.cartFormItems),
      loadingEl = jQuery('<div class="' + ids.loadingEl + '" />').hide().appendTo(cartFormItems);

  cartFormItems.addClass('has-loading is-animated');
  cartFormItems.css({'height': cartFormItems.height() + 'px'});
  loadingEl.show();
  cartFormItems.load('/cart ' + selectors.cartFormItemsContent, function () {
    loadingEl.hide();
    cartFormItems.animateAuto('height', 500);
    cartFormItems.removeClass('has-loading');
    if (typeof callback === 'function') {
      callback();
    }
  });

  this.reloadCartSummary();
  this.reloadCartSubtotal();
}

/*
 * Reload cart summary.
 */
Yi3Cart.prototype.reloadCartSummary = function () {
  var selectors = {
        cart: '.cartsummary',
        cartContent: '.cartsummary-wrap'
      };

  this.reloadCartUi(selectors);
}

/*
 * Reload cart subtotal.
 */
Yi3Cart.prototype.reloadCartSubtotal = function () {
  var selectors = {
        cart: '#subtotal .finalinfo',
        cartContent: '#subtotal .finalinfo-wrap'
      };

  this.reloadCartUi(selectors);
}

/*
 * Reload any cart ui.
 *
 * @param {Object} [selectors] An object containing the selectors:
 * {
 *   cart: '#subtotal .finalinfo',
 *   cartContent: '#subtotal .finalinfo-wrap'
 * }
 */
Yi3Cart.prototype.reloadCartUi = function (selectors) {
  var ids = {
        loadingEl: 'yi3---loading'
      },
      cart = jQuery(selectors.cart),
      loadingEl = jQuery('<div class="' + ids.loadingEl + '" />').appendTo(cart);

  cart.addClass('has-loading');
  loadingEl.show();
  cart.load('/cart ' + selectors.cartContent, function () {
    loadingEl.hide();
    cart.removeClass('has-loading');
  });
}

/* ERROR UI */

/*
 * Add error class.
 *
 * @param {Function} [element] The element to add the class to.
 */
Yi3Cart.prototype.setError = function (element) {
  element.addClass('error');
}

/*
 * Remove error class.
 *
 * @param {Function} [element] The element to add the class to.
 */
Yi3Cart.prototype.unsetError = function (element) {
  element.removeClass('error');
}

/* INITIALIZE */

/*
 * Initialize class.
 */
Yi3Cart.prototype.ini = function () {
  this.handleAddToCart('form #add-to-cart');
}

/**********************************************************************
                              INITIALIZE
**********************************************************************/

Yi3 = {
  Cart: new Yi3Cart()
};
Yi3.Cart.ini();
